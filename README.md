# Sobre

Teste desenvolvido para empresa Softruck consumindo as API's do FourSquare e Google Maps, com o objetivo de mostrar locais em um mapa de acordo com dados inseridos pelo usuário, dados estes como:
* local
* área de alcance e categoria
Dando-lhes ainda a opção de ver os 5 lugares mais recomendados e mais perto do usuário e um mapa de calor dos locais marcados no mapa. 

# Instalação

Baixe o projeto pelo seguinte comando:

```
$ git clone https://Gabriel949@bitbucket.org/Gabriel949/test_softruck.git
```

Navegue para dentro da pasta:

```
cd test_softruck
```

Para instalar as dependências do projeto  basta executar o comando abaixo dentro do diretório do projeto 

```
$ npm i
```

# Uso

Para rodar o projeto execute o comando abaixo:

```
$ npm start
```
Ele executara o projeto na porta 8080.